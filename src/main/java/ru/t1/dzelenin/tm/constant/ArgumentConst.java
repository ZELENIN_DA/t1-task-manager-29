package ru.t1.dzelenin.tm.constant;

import org.jetbrains.annotations.NotNull;

public final class ArgumentConst {

    @NotNull
    public static final String HELP = "-h";

    @NotNull
    public static final String VERSION = "-v";

    @NotNull
    public static final String ABOUT = "-a";

    @NotNull
    public static final String INFO = "-i";

    @NotNull
    public static final String COMMANDS = "-cmd";

    @NotNull
    public static final String ARGUMENTS = "-arg";

}
