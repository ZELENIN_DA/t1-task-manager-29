package ru.t1.dzelenin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        getProjectService().changeProjectStatusId(userId, id, Status.COMPLETED);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-change-status-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Complete project by id.";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
