package ru.t1.dzelenin.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.dto.Domain;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

public final class DataBackupSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "backup-save";
    @NotNull
    public static final String DESCRIPTION = "Save backup to file.";

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull
        final Domain domain = getDomain();
        @NotNull
        final File file = new File(FILE_BACKUP);
        @NotNull
        final Path path = file.toPath();
        Files.deleteIfExists(path);
        Files.createFile(path);
        @NotNull
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @NotNull
        final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);

        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        byteArrayOutputStream.close();

        @NotNull
        final byte[] bytes = byteArrayOutputStream.toByteArray();
        @NotNull
        final String base64 = new BASE64Encoder().encode(bytes);
        @NotNull
        final FileOutputStream fileOutputStream = new FileOutputStream(file);

        fileOutputStream.write(base64.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
