package ru.t1.dzelenin.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public final class DataJsonSaveJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-json-jaxb";
    @NotNull
    public static final String DESCRIPTION = "Save data in json file.";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE JSON]");

        System.setProperty(CONTEXT_FACTORY, CONTEXT_FACTORY_JAXB);
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_JSON);
        @NotNull final Path path = file.toPath();

        Files.deleteIfExists(path);
        Files.createFile(path);

        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.setProperty(MEDIA_TYPE, APPLICATION_TYPE_JSON);
        jaxbMarshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}


